import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:session1_3/auth/data/repository/show_error.dart';
import 'package:session1_3/auth/data/repository/supabase.dart';
import 'package:session1_3/auth/presentation/pages/sign_up_page.dart';
import 'package:session1_3/auth/presentation/widgets/text_field.dart';
import 'package:session1_3/home/presentation/pages/home_page.dart';

class Change_pass extends StatefulWidget {
  const Change_pass({super.key});

  @override
  State<Change_pass> createState() => _Change_passState();
}

class _Change_passState extends State<Change_pass> {
  var confirm_password_controller = TextEditingController();
  var password_controller = TextEditingController();
  bool button = false;
  bool password_obscure = true;
  bool confirm_password_obscure = true;
  void is_Valid(){
    setState(() {
      button = password_controller.text.isNotEmpty && confirm_password_controller.text.isNotEmpty;
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 155,),
              Text(
                "New Password",
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                  "Enter new password",
                  style: Theme.of(context).textTheme.titleMedium
              ),
              SizedBox(height: 87,),
              Custom_Field(label: "Password", hint: "**********", controller: password_controller, onchange: (newText){is_Valid();}, is_obscure: password_obscure, on_tap: (){
                setState(() {
                  password_obscure = !password_obscure;
                });
              },),
              SizedBox(height: 24,),
              Custom_Field(label: "Confirm Password", hint: "**********", controller: confirm_password_controller, onchange: (newText){is_Valid();}, is_obscure: confirm_password_obscure, on_tap: (){
                setState(() {
                  confirm_password_obscure = !confirm_password_obscure;
                });
              },),
              SizedBox(height: 54,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    onPressed: (button)?()async{
                      if (confirm_password_controller.text == password_controller.text){
                        await change_password(
                            password: password_controller.text,
                            onResponse: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => Home_page()));
                            },
                            onError: (String e){showError(context, e);});
                      }
                      else{
                        showError(context, "Password do not match");
                      }
                    }:null,
                    child: Text("Log in",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w700
                      ),),
                    style: Theme.of(context).filledButtonTheme.style,
                  ),
                ),
              ),
              SizedBox(height: 264,)

            ],
          ),
        ),
      ),
    );
  }
}