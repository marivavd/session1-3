
import 'package:flutter/material.dart';
import 'package:session1_3/auth/data/repository/show_error.dart';
import 'package:session1_3/auth/data/repository/supabase.dart';
import 'package:session1_3/auth/presentation/pages/otp.dart';
import 'package:session1_3/auth/presentation/pages/sign_in_page.dart';
import 'package:session1_3/auth/presentation/widgets/text_field.dart';

class Forgot_pass extends StatefulWidget {
  const Forgot_pass({super.key});

  @override
  State<Forgot_pass> createState() => _Forgot_passState();
}

class _Forgot_passState extends State<Forgot_pass> {
  var email_controller = TextEditingController();
  bool button = false;
  void is_Valid(){
    setState(() {
      button = email_controller.text.isNotEmpty;
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 155,),
              Text(
                "Forgot Password",
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                  "Enter your email address",
                  style: Theme.of(context).textTheme.titleMedium
              ),
              SizedBox(height: 56,),

              Custom_Field(label: "Email Address", hint: "***********@mail.com", controller: email_controller, onchange: (newText){is_Valid();}),
              SizedBox(height: 59,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    onPressed: (button)?()async{
                      await send_email(
                          email: email_controller.text,
                          onResponse: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => OTP(email: email_controller.text)));
                          },
                          onError: (String e){showError(context, e);});
                    }:null,
                    child: Text("Send OTP",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w700
                      ),),
                    style: Theme.of(context).filledButtonTheme.style,
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Remember password? Back to ",
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
                  ),
                  InkWell(
                    child: Text(
                      "Sign in",
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Color(0xFF0560FA)),
                    ),
                    onTap: (){
                      setState(() {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_in_Page()));
                      });
                    },
                  )
                ],
              ),
              SizedBox(height: 373,)

            ],
          ),
        ),
      ),
    );
  }
}