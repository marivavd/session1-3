
import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';
import 'package:session1_3/auth/data/repository/show_error.dart';
import 'package:session1_3/auth/data/repository/supabase.dart';
import 'package:session1_3/auth/presentation/pages/change_password.dart';
import 'package:session1_3/auth/presentation/pages/sign_in_page.dart';
import 'package:session1_3/auth/presentation/widgets/text_field.dart';

class OTP extends StatefulWidget {
  final String email;
  const OTP({super.key, required this.email});

  @override
  State<OTP> createState() => _OTPState();
}

class _OTPState extends State<OTP> {
  var controller = TextEditingController();
  bool button = false;
  bool is_Error = false;

  @override
  Widget build(BuildContext context) {
    double widthScreen = MediaQuery
        .of(context)
        .size
        .width;
    double separator = widthScreen / 6 - 32;

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 158,),
              Text(
                "OTP Verification",
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                  "Enter the 6 digit numbers sent to your email",
                  style: Theme.of(context).textTheme.titleMedium
              ),
              SizedBox(height: 52,),
              Pinput(
                length: 6,
                controller: controller,
                separatorBuilder: (context) => SizedBox(width: separator,),
                defaultPinTheme: PinTheme(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                        border: Border.all(color: Color(0xFFA7A7A7)),
                        borderRadius: BorderRadius.zero
                    )
                ),
                focusedPinTheme: PinTheme(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                        border: Border.all(color: Color(0xFF0560FA)),
                        borderRadius: BorderRadius.zero
                    )
                ),
                submittedPinTheme: (!is_Error) ? PinTheme(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                        border: Border.all(color: Color(0xFF0560FA)),
                        borderRadius: BorderRadius.zero
                    )
                ) : PinTheme(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                        border: Border.all(color: Color(0xFFED3A3A)),
                        borderRadius: BorderRadius.zero
                    )
                ),
                onChanged: (text) {
                  setState(() {
                    is_Error = false;
                    button = text.length == 6;
                  });
                },


              ),
              SizedBox(height: 30,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("If you didn’t receive code, ", style: Theme
                      .of(context)
                      .textTheme
                      .titleMedium
                      ?.copyWith(fontWeight: FontWeight.w400),),
                  InkWell(
                    onTap: () async {
                      await send_email(
                          email: widget.email,
                          onResponse: () {},
                          onError: (String e) {
                            showError(context, e);
                          });
                    },
                    child: Text(
                      "resend",
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                          ?.copyWith(color: Color(0xFF0560FA)),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 56,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    style: Theme
                        .of(context)
                        .filledButtonTheme
                        .style,
                    onPressed: (button) ? () async {
                      await verify_OTP(
                          email: widget.email,
                          code: controller.text,
                          onResponse: () {
                            Navigator.push(context, MaterialPageRoute(
                                builder: (context) => Change_pass()));
                          },
                          onError: (String e) {
                            showError(context, e);
                          });
                    } : null,
                    child: Text(
                      "Set New Password",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w700
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 350,)



            ],
          ),
        ),
      ),
    );
  }
}