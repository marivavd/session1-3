import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:session1_3/auth/data/repository/show_error.dart';
import 'package:session1_3/auth/data/repository/supabase.dart';
import 'package:session1_3/auth/presentation/pages/forgot_password.dart';
import 'package:session1_3/auth/presentation/pages/sign_up_page.dart';
import 'package:session1_3/auth/presentation/widgets/text_field.dart';
import 'package:session1_3/home/presentation/pages/home_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Sign_in_Page extends StatefulWidget {
  const Sign_in_Page({super.key});

  @override
  State<Sign_in_Page> createState() => _Sign_in_PageState();
}

class _Sign_in_PageState extends State<Sign_in_Page> {
  SharedPreferences? sharedPreferences;
  var email_controller = TextEditingController();
  var password_controller = TextEditingController();
  bool check = false;
  bool button = false;
  String initials = '';
  bool password_obscure = true;
  void is_Valid(){
    setState(() {
      button = email_controller.text.isNotEmpty && password_controller.text.isNotEmpty;
    });

  }
  @override
  void initState(){
    super.initState();
    SharedPreferences.getInstance().then((value) async {
      sharedPreferences = value;
      initials = await getCheck();
      if (initials != ''){
        await logining();
      }
    });

  }
  Future<String> getCheck() async {
      return sharedPreferences!.getString("action") ?? '';
  }
   Future<void> logining()async{
    print(initials.split('?')[0]);
     await sign_in(
         email: initials.split('?')[0],
         password: initials.split('?')[1],
         onResponse: (){
           Navigator.push(context, MaterialPageRoute(builder: (context) => Home_page()));
         },
         onError: (String e){showError(context, e);});
   }
  void _incrementCounter() async {
    setCounter(email_controller.text, password_controller.text);
  }
  Future<void> setCounter(String email, String password) async {
    sharedPreferences!.setString("action", "${email}?${password}");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 155,),
              Text(
                "Welcome Back",
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                  "Fill in your email and password to continue",
                  style: Theme.of(context).textTheme.titleMedium
              ),
              SizedBox(height: 20,),

              Custom_Field(label: "Email Address", hint: "***********@mail.com", controller: email_controller, onchange: (newText){is_Valid();}),
              SizedBox(height: 24,),
              Custom_Field(label: "Password", hint: "**********", controller: password_controller, onchange: (newText){is_Valid();}, is_obscure: password_obscure, on_tap: (){
                setState(() {
                  password_obscure = !password_obscure;
                });
              },),
              SizedBox(height: 37,),
              Row(
                children: [
                  Expanded(
                      child: Row(
                        children: [
                          SizedBox(width: 1,),
                          SizedBox(
                            height: 14,
                            width: 14,
                            child: Checkbox(
                              value: check,
                              onChanged: (bool? val){
                                setState(() {
                                  check = val!;
                                  is_Valid();
                                });
                              },
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(2)
                              ),
                              side: BorderSide(
                                  color: Color(0xFFA7A7A7),
                                  width: 1,
                              ),
                              activeColor: Color(0xFF006CEC),
                            ),
                          ),
                          SizedBox(width: 4,),
                          Text(
                            "Remember password",
                            style: TextStyle(
                              color: Color(0xFFA7A7A7),
                              fontSize: 12,
                              fontWeight: FontWeight.w500
                            )
                          )
                        ],
                      )),
                  InkWell(
                    onTap: (){
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const Forgot_pass())
                      );
                    },
                    child: Text(
                      "Forgot Password?",
                        style: TextStyle(
                            color: Color(0xFF0560FA),
                            fontSize: 12,
                            fontWeight: FontWeight.w500
                        )
                    ),
                  )
                ],
              ),
              SizedBox(height: 187,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    onPressed: (button)?()async{
                      await sign_in(
                          email: email_controller.text,
                          password: password_controller.text,
                          onResponse: (){
                            if (check){
                              _incrementCounter();
                            }
                            Navigator.push(context, MaterialPageRoute(builder: (context) => Home_page()));
                          },
                          onError: (String e){showError(context, e);});
                    }:null,
                    child: Text("Log in",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w700
                      ),),
                    style: Theme.of(context).filledButtonTheme.style,
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Already have an account?",
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
                  ),
                  InkWell(
                    child: Text(
                      "Sign Up",
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Color(0xFF0560FA)),
                    ),
                    onTap: (){
                      setState(() {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_up_Page()));
                      });
                    },
                  )
                ],
              ),
              SizedBox(height: 18,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Text('or sign in using', style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),),
                      SizedBox(height: 8,),
                      Image.asset("assets/Facebook google, apple.png")
                    ],
                  )
                ],
              ),
              SizedBox(height: 95,)

            ],
          ),
        ),
      ),
    );
  }
}