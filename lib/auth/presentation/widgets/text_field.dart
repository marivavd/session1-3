
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class Custom_Field extends StatelessWidget{
  final String label;
  final String hint;
  final TextEditingController controller;
  final Function(String?) onchange;
  final bool is_obscure;
  final Function()? on_tap;
  final MaskTextInputFormatter? formatter;
  const Custom_Field({super.key, required this.label, required this.hint, required this.controller, required this.onchange, this.is_obscure = false, this.on_tap, this.formatter});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(label,
        style: Theme.of(context).textTheme.titleMedium,),
        SizedBox(height: 8,),
        SizedBox(
          height: 44,
          child: TextField(
            controller: controller,
            inputFormatters: (formatter != null)?[formatter!]: [],
            onChanged: onchange,
            obscureText: is_obscure,
            obscuringCharacter: '*',
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 14, horizontal: 10),
              hintText: hint,
              hintStyle: TextStyle(
                color: Color(0xFFCFCFCF),
                fontSize: 14,
                fontWeight: FontWeight.w500
              ),
              border: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Color(0xFFA7A7A7),
                  width: 1
                ),
                borderRadius: BorderRadius.circular(4)
              ),
              suffixIcon: (on_tap != null)?GestureDetector(
                onTap: on_tap,
                child: Image.asset("assets/eye-slash.png"),
              ): null
            ),
          ),
        )
      ],
    );
  }

}