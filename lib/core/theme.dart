import 'package:flutter/material.dart';

var lightTheme =ThemeData(
  textTheme: TextTheme(
    titleLarge: TextStyle(
        color: Color(0xFF3A3A3A),
        fontSize: 24,
        fontWeight: FontWeight.w500,
      fontFamily: 'Roboto'
    ),
    titleMedium: TextStyle(
        color: Color(0xFFA7A7A7),
        fontSize: 14,
        fontWeight: FontWeight.w500,
        fontFamily: 'Roboto'
    )
  ),
  filledButtonTheme: FilledButtonThemeData(
    style: FilledButton.styleFrom(
        backgroundColor: Color(0xFF0560FA),
        disabledBackgroundColor: Color(0xFFA7A7A7),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(4),
        )),
  )
);