import 'package:session1_3/onboarding/data/repository/get_onboarding_items.dart';

class Queue{
  final List<Map<String, String>> sp = [];

  void push(Map<String, String> elem){
    sp.insert(sp.length, elem);

  }

  bool is_empty(){
    return sp.isEmpty;
  }

  int length(){
    return sp.length;
  }

  Map<String, String> current_el(){
    return sp[0];

  }
  void delete(){
    sp.remove(sp[0]);
  }

  void reset_queue(){
    sp.clear();
    for (var elem in get_onboarding_items()){
      sp.add(elem);
    }


  }
}