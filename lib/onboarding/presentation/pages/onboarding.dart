import 'dart:collection';
import 'package:session1_3/auth/presentation/pages/sign_in_page.dart';
import 'package:session1_3/auth/presentation/pages/sign_up_page.dart';
import 'package:session1_3/onboarding/domain/queue.dart';
import 'package:flutter/material.dart';
import 'package:session1_3/onboarding/presentation/pages/holder.dart';
import 'package:shared_preferences/shared_preferences.dart';


class OnBoarding extends StatefulWidget {
  const OnBoarding({super.key});

  @override
  State<OnBoarding> createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {
  int _counter = 0;
  Queue queue = Queue();
  SharedPreferences? sharedPreferences;
  Map<String, String> current_element = {};

  Future<int> getCounter() async {
    return sharedPreferences!.getInt("counter") ?? 0;
  }

  Future<void> setCounter(int newCounter) async {
    sharedPreferences!.setInt("counter", newCounter);
  }

  @override
  void initState(){
    super.initState();

    SharedPreferences.getInstance().then((value) async {
      sharedPreferences = value;
      _counter = await getCounter();
      queue.reset_queue();
      for(int i = 0; i < _counter; i++){
        queue.delete();
      }
      if (queue.is_empty()){
        setState(() {
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const Sign_up_Page())
          );
        });
      }else{
        setState(() {
          current_element = queue.current_el();
        });
      }
    });
  }

  void _incrementCounter() async {
    _counter ++;
    setCounter(_counter);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 111,),
            Align(
              alignment: Alignment.center,
              child: Image.asset(current_element["image"]!),
            ),
            SizedBox(height: 48,),
            Text(
              current_element['title']!,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 24,
                color: Color(0xFF0560FA),
                fontWeight: FontWeight.w700
              ),
            ),
            SizedBox(height: 5,),
            Text(
              current_element['text']!,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 16,
                  color: Color(0xFF3A3A3A),
                  fontWeight: FontWeight.w400
              ),
            ),
            SizedBox(height: 82,),
            (queue.length() == 1) ?
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Align(
                          alignment: Alignment.center,
                          child: SizedBox(
                            height: 46,
                            width: 342,
                            child: FilledButton(
                              style: FilledButton.styleFrom(
                                  backgroundColor: Color.fromARGB(255, 5, 96, 250),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(4)
                                  )),
                              onPressed: (){
                                setState(() {
                                  queue.delete();
                                  _incrementCounter();
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_up_Page()));
                                });
                              },
                              child: Text(
                                'Sign Up',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 20,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Already have an account?",
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
                            ),
                            InkWell(
                              child: Text(
                                "Sign in",
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Color(0xFF0560FA)),
                              ),
                              onTap: (){
                                setState(() {
                                  queue.delete();
                                  _incrementCounter();
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_in_Page()));
                                });
                              },
                            )
                          ],
                        )
                      ],
                    ),
                  )
                :
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        height: 50,
                        width: 100,
                        child: FilledButton(
                            onPressed: (){
                              setState(() {
                                for (int i = 0; i < queue.length(); i++){
                                  queue.delete();
                                  _incrementCounter();
                                }
                                Navigator.push(context, MaterialPageRoute(builder: (context) => Holder()));

                              });
                            },
                            style: FilledButton.styleFrom(
                                backgroundColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4.69),
                                    side: BorderSide(
                                        color: Color(0xFF0560FA),
                                        width: 1
                                    )
                                )),
                            child: Text(
                              "Skip",
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Color(0xFF0560FA), fontWeight: FontWeight.w700),
                            )),
                      ),
                      SizedBox(
                        height: 50,
                        width: 100,
                        child: FilledButton(
                            onPressed: (){
                              setState(() {
                                queue.delete();
                                current_element = queue.current_el();
                                _incrementCounter();
                              });
                            },
                            style: FilledButton.styleFrom(
                                backgroundColor: Color(0xFF0560FA),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.69),
                                )),
                            child: Text(
                              "Next",
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Colors.white, fontWeight: FontWeight.w700),
                            )),
                      )
                    ],
                  ),
                ),
            SizedBox(height: (queue.is_empty()) ? 64 : 99)
          ],
        ),
      ),

    );
  }
}
