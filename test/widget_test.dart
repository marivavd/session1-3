// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:session1_3/onboarding/data/repository/get_onboarding_items.dart';
import 'package:session1_3/onboarding/domain/queue.dart';
import 'package:session1_3/onboarding/presentation/pages/holder.dart';
import 'package:session1_3/onboarding/presentation/pages/onboarding.dart';

import 'package:session1_3/run.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  Queue queue = Queue();
  group("Тестирование OnBoarding", () {
    test("−	Изображение и текста из очереди извлекается правильно (в порядке добавления в очередь).", () {
      var sp = get_onboarding_items();
      queue.reset_queue();
      for (var val in sp){
        expect(queue.current_el(), val);
        queue.delete();
      }
    });
    test("−	Корректное извлечение элементов из очереди (количество элементов в очереди уменьшается на единицу).", () {
      queue.reset_queue();
      var len_sp = queue.length();
      var sp = get_onboarding_items();
      for (var val in sp){
        queue.delete();
        expect(queue.length(), len_sp - 1);
        len_sp = queue.length();
      }
    });
    testWidgets("−	В случае, когда в очереди несколько картинок, устанавливается правильная надпись на кнопке.", (widgetTester) async{
      queue.reset_queue();
      final pageview = find.byType(PageView);
      await widgetTester.runAsync(() => widgetTester.pumpWidget(const MaterialApp(
        home: OnBoarding(),
      )));
      var elem = queue.current_el();
      await widgetTester.pumpAndSettle();
      expect(find.widgetWithText(FilledButton, "Next"), findsOneWidget);
      final titletext = find.text(elem['title']!);
      await widgetTester.dragUntilVisible(titletext, pageview, const Offset(-250, 0));
      queue.delete();
      elem = queue.current_el();
      await widgetTester.pumpAndSettle();
      expect(find.widgetWithText(FilledButton, "Next"), findsOneWidget);
    });
    testWidgets("−	Случай, когда очередь пустая, надпись на кнопке должна измениться на Sing Up.", (widgetTester) async{
      queue.reset_queue();
      final pageview = find.byType(PageView);

      await widgetTester.runAsync(() => widgetTester.pumpWidget(const MaterialApp(
        home: OnBoarding(),
      )));

      while(queue.length() > 1){
        if(queue.length() == 1){
          await widgetTester.pumpAndSettle();
          expect(await find.widgetWithText(FilledButton, "Sign Up"), findsOneWidget);}
        queue.delete();
      }



    });
    testWidgets("−	Если очередь пустая и пользователь нажал на кнопку “Sing Up”, происходит открытие пустого экрана «Holder» приложения. Если очередь не пустая – переход отсутствует.", (widgetTester) async{
      queue.reset_queue();
      final pageview = find.byType(PageView);
      await widgetTester.runAsync(() => widgetTester.pumpWidget(const MaterialApp(
        home: OnBoarding(),
      )));
      while(queue.length() > 1){

        if (queue.length() == 1){
          await widgetTester.pumpAndSettle();
          await widgetTester.tap(find.widgetWithText(FilledButton, "Sign Up"));
          expect(find.byElementType(Holder), findsOneWidget);
        }
        queue.delete();

      }

    });
  });
}
